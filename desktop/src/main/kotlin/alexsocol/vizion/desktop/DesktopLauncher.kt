package alexsocol.vizion.desktop

import alexsocol.vizion.*
import alexsocol.vizion.util.P_img
import com.badlogic.gdx.Files
import com.badlogic.gdx.backends.lwjgl.*

object DesktopLauncher {

	@JvmStatic
	fun main(args: Array<String>) {
		val config = LwjglApplicationConfiguration()
		config.title = "$name v$ver"
		config.width = 1280
		config.height = 720
		config.resizable = false
		
		config.addIcon("$P_img/icon128.png", Files.FileType.Internal)
		config.addIcon("$P_img/icon32.png", Files.FileType.Internal)
		config.addIcon("$P_img/icon16.png", Files.FileType.Internal)
		
		LwjglApplication(main, config)
	}
}