package alexsocol.vizion.util

import com.badlogic.gdx.Gdx
import java.text.SimpleDateFormat
import java.util.*



private val sdf_time = SimpleDateFormat("HH:mm:ss")
private val sdf_date_time = SimpleDateFormat("MM:dd:YY HH:mm:ss")
private fun format(msg: Any?, level: String) = "[${sdf_time.format(Calendar.getInstance().time)}] [${Thread.currentThread().name}/$level] $msg"

fun log(msg: Any?) = Gdx.app.log("ViZioN", format(msg, "INFO"))
fun deb(msg: Any?) = Gdx.app.debug("ViZioN", format(msg, "DEBUG"))
fun err(msg: Any?) = Gdx.app.error("ViZioN", format(msg, "ERROR"))