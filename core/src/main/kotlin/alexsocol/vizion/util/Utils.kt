package alexsocol.vizion.util

import alexsocol.vizion.main
import com.badlogic.gdx.audio.Music
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.round

val sdf_dt = SimpleDateFormat("MM.dd.yy HH:mm:ss")
fun formatDate(): String = sdf_dt.format(Date())

private var prevX = 18f
private var prevY = 18f

fun BitmapFont.setLineHeight(height: Float) {
	prevX = scaleX
	prevY = scaleY
	data.setScale(height * scaleY / lineHeight)
}

fun BitmapFont.prevLineHeight() = data.setScale(prevX, prevY)

fun drawBg(bg: TextureRegion) = main.batch.draw(bg, 0f, 0f, main.settings.width, main.settings.height)

fun Color.fade(f: Float) = cpy().mul(f)!!

data class MutableInt(var i: Int)
data class MutableBoolean(var b: Boolean)

fun Music.play(loop: Boolean) {
	isLooping = loop
	play()
}

fun mapRange(input_start: Float, input_end: Float, output_start: Float, output_end: Float, input: Float): Float {
	val slope = 1.0f * (output_end - output_start) / (input_end - input_start)
	return output_start + round(slope * (input - input_start))
}

/**
 * Makes a list of [Pair]s from original [Iterable] (zero to first, second to third, etc)
 *
 * If there are odd amount of elements - then last pair will have either last element to [last] argument or same element in both positions if [last] is null/unprovided
 */
fun <T> Iterable<T>.paired(last: T? = null): List<Pair<T, T>> {
	val pairs = ArrayList<Pair<T, T>>()
	val i = this.iterator()
	while (i.hasNext()) {
		val a = i.next()
		val b = if (i.hasNext()) i.next() else last ?: a
		pairs.add(a to b)
	}
	return pairs
}

/**
 * Returns a list containing first [n] elements, removed from this iterable.
 *
 * @throws IllegalArgumentException if [n] is negative.
 */
fun <T> MutableIterable<T>.retainFirst(n: Int): List<T> {
	require(n >= 0) { "Requested element count $n is less than zero." }
	if (n == 0) return emptyList()
	if (n == 1) {
		val iterator = iterator()
		if (!iterator.hasNext())
			throw NoSuchElementException("Collection is empty.")
		return listOf(iterator.next()).also { iterator.remove() }
	}
	if (this is MutableCollection<T>) {
		if (n >= size) return toList().also { this.clear() }
	}
	var count = 0
	val list = ArrayList<T>(n)
	while (count++ < n) {
		val iterator = iterator()
		if (!iterator.hasNext()) break
		list.add(iterator.next()).also { iterator.remove() }
	}
	return list.optimizeReadOnlyList()
}

fun <T> List<T>.optimizeReadOnlyList() = when (size) {
	0 -> emptyList()
	1 -> listOf(this[0])
	else -> this
}