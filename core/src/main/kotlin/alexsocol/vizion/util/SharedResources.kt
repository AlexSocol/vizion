package alexsocol.vizion.util

import alexsocol.vizion.P_ass
import alexsocol.vizion.engine.handler.SettingsHandler
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.*
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.*

const val P_img = "$P_ass/textures"
const val P_scene = "$P_img/backgrounds"
const val P_sprite = "$P_img/sprites"

enum class TextureType {
	IMG, BG
}

object SharedResources {
	
	private val imgs = hashMapOf<String, TextureRegion>()
	private val bgs = hashMapOf<String, TextureRegion>()
	private val sprites = hashMapOf<String, SpritePosition>()
	private val sounds = hashMapOf<String, Sound>()
	private val musics = hashMapOf<String, Music>()
	
	val textArea = TextureRegion(Texture("$P_img/gui/textarea.png"))
	val textAreaWithSpeaker = TextureRegion(Texture("$P_img/gui/textareasaid.png"))
	
	init {
		textArea.flip(false, true)
		textAreaWithSpeaker.flip(false, true)
	}
	
	private fun registerTexture(name: String, type: TextureType): TextureRegion {
		return when (type) {
			TextureType.IMG -> TextureRegion(Texture("$P_img/$name.png"))	.also { it.flip(false, true) }.also { imgs[name] = it }
			TextureType.BG  -> TextureRegion(Texture("$P_scene/$name.png"))	.also { it.flip(false, true) }.also { bgs[name] = it }
		}
	}
	
	private data class SpritePosition(val sprite: Sprite, var x: Float, var y: Float)
	
	private fun registerSprite(name: String, x: Float, y: Float): Sprite {
		val pos = SpritePosition(Sprite(Texture("$P_sprite/$name.png")), x, y)
		pos.sprite.flip(false, true)
		pos.resize()
		sprites[name] = pos
		return pos.sprite
	}
	
	private fun registerSound(name: String): Sound {
		val s = Gdx.audio.newSound(Gdx.files.internal("$P_ass/sound/$name"))
		sounds[name] = s
		return s
	}
	
	private fun registerMusic(name: String): Music {
		val m = Gdx.audio.newMusic(Gdx.files.internal("$P_ass/music/$name"))
		m.volume = SettingsHandler.volume / 100f
		musics[name] = m
		return m
	}

	
	/**
	 * Returns any cached image, caches it if first created
	 */
	fun img(name: String) = imgs[name] ?: registerTexture(name, TextureType.IMG)
	
	/**
	 * Returns cached background image, caches it if first created
	 */
	fun bg(name: String) = bgs[name] ?: registerTexture(name, TextureType.BG)
	
	/**
	 * Returns cached sprite, caches it if first created
	 */
	fun sprite(name: String, x: Float = 0f, y: Float = 0f): Sprite = sprites[name]?.sprite ?: registerSprite(name, x, y)
	
	fun sound(name: String) = sounds[name] ?: registerSound(name)
	
	fun music(name: String) = musics[name] ?: registerMusic(name)
	
	fun deleteSprite(name: String) {
		sprites.remove(name)?.let { it.sprite.texture.dispose() }
	}
	
	/**
	 * Returns cached sprite, caches it if first created
	 */
	fun writeSprite(sb: StringBuilder, name: String) {
		val sprite = sprites[name] ?: return
		sb.append(name).append('\n')
		sb.append(sprite.x).append(' ').append(sprite.y).append('\n')
	}
	
	/**
	 * Resizes all sprites to match new resolution
	 */
	fun resizeSprites() = sprites.forEach { it.value.resize() }
	
	fun resize(name: String, x: Float, y: Float) {
		val sPos = sprites[name] ?: return
		sPos.x = x
		sPos.y = y
		sPos.resize()
	}
	
	private fun SpritePosition.resize() {
		val w = SettingsHandler.width / 1920
		val h = SettingsHandler.height / 1080
		sprite.setBounds(x * w, y * h, sprite.texture.width * w, sprite.texture.height * h)
	}
	
	fun adjustVolume(vol: Int) {
		musics.forEach { it.value.volume = vol / 100f }
	}
	
	fun dispose() {
		textArea.texture.dispose()
		textAreaWithSpeaker.texture.dispose()
		
		imgs.forEach { it.value.texture.dispose() }
		bgs.forEach { it.value.texture.dispose() }

		sprites.forEach { it.value.sprite.texture.dispose() }
		
		sounds.forEach { it.value.stop(); it.value.dispose() }
		musics.forEach { it.value.stop(); it.value.dispose() }
	}
}
