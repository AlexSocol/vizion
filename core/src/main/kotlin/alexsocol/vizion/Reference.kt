package alexsocol.vizion

const val name = "ViZioN"
const val major = "1"
const val minor = "2"
const val build = "27"
const val ver = "$major.$minor.$build"

const val P_ass = "assets"

const val P_lang = "$P_ass/lang"
const val P_scripts = "$P_ass/scripts"

const val copyright = "ViZioN @ by AlexSocol @ 2020"