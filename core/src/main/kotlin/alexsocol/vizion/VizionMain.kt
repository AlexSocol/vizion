package alexsocol.vizion

import alexsocol.vizion.engine.gui.IGui
import alexsocol.vizion.engine.gui.gameprocess.*
import alexsocol.vizion.engine.handler.*
import alexsocol.vizion.util.*
import com.badlogic.gdx.*
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.*
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator

val main get() = VizionMain

object VizionMain: ApplicationAdapter() {
	
	lateinit var batch: SpriteBatch
	lateinit var camera: OrthographicCamera
	
	val settings get() = SettingsHandler
	// lateinit var saves: SaveManager
	
	lateinit var font: BitmapFont
	lateinit var gameFont: BitmapFont
	lateinit var glyph: GlyphLayout
	lateinit var input: InputHandler
	
			 var prevGui: IGui? = null
	private  set
	lateinit var gui: IGui
	private  set
	
	/** Actual screen size width */
	var width = 1280f
	/** Actual screen size height */
	var height = 720f
	
	/** Debug lines ontop of all guis */
	var debug = false
	
	var ended = false
	
	/**
	 * @param store Do store prev gui
	 */
	fun setCurrentGui(new: IGui, store: Boolean = true) {
		debug("Gui set to ${new::class.java.name}")
		
		if (main::gui.isInitialized) {
			prevGui = if (store)
				gui
			else {
				gui.onDispose()
				null
			}
		}
		
		gui = new
		gui.onLoad()
		gui.onResize()
	}
	
	fun prevGui(): Boolean {
		if (prevGui == null) return false
		
		debug("Gui returned to ${prevGui!!::class.java.name}")
		if (main::gui.isInitialized) gui.onDispose()
		
		gui = prevGui ?: throw ConcurrentModificationException("Prev GUI was checked to be not null, but after that it appeared to be null")
		prevGui = null
		return true
	}
	
	override fun create() {
		setCurrentGui(GuiInit())
		
		input = InputHandler()
		Gdx.input.inputProcessor = input
		
		camera = OrthographicCamera()
		camera.setToOrtho(true, settings.width, settings.height)
		
		batch = SpriteBatch()
		glyph = GlyphLayout()
		
		reloadFont("en_US")
	}
	
	override fun render() {
		super.render()
		
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
		
		camera.update()
		
		batch.projectionMatrix = camera.combined
		batch.begin()
		gui.draw()
		
		if (debug) infoLayer()
		
		batch.end()
		
		if (initStage >= maxStages) return
		
		// when adding stages don't forget to draw them in GuiInit
		when (initStage++) {
			1 -> SettingsHandler.read()
			2 -> SavingHandler.read()
			3 -> LanguageHandler.readLangs(SettingsHandler.lang)
			4 -> reloadFont(SettingsHandler.lang)
		}
	}
	
			var initStage = 0
	const	val maxStages = 5
	
	private var f = 25f
			get() {
				return field.also { field += 20 }
			}
	
	private fun infoLayer() {
		val was = font.color.cpy()
		font.color = Color.SCARLET
		
		val w = settings.width / 1920
		val h = settings.height / 1080
		
		font.draw(batch, settings.toString(), 5f, f)
		font.draw(batch, "Gui: ${gui::class.java.name}", 5f, f)
		font.draw(batch, "Pointer: x: ${Gdx.input.x} y: ${Gdx.input.y}", 5f, f)
		font.draw(batch, "Pointer relative: x: ${Gdx.input.x / w} y: ${Gdx.input.y / h}", 5f, f)
		font.draw(batch, "FPS: ${Gdx.graphics.framesPerSecond}", 5f, f)
		
		f = 25f
		
		for ((k, v) in SceneHandler.userVariables) {
			glyph.setText(font, "$k = $v")
			font.draw(batch, glyph, settings.width - glyph.width - 5f, f)
		}
		
		f = 25f
		
		font.color = was
	}
	
	override fun resize(w: Int, h: Int) {
		super.resize(w, h)
		
		width = w.toFloat()
		height = h.toFloat()
		
		gui.onResize()
	}
	
	override fun pause() {
		debug("Paused")
		if (gui is GuiIngame) SavingHandler.autoSave()
	}
	
	override fun resume() {
		debug("Resumed")
	}
	
	override fun dispose() {
		gui.onDispose()
		batch.dispose()
		font.dispose()
		gameFont.dispose()
		SharedResources.dispose()
	}
	
	fun quit(): Boolean {
		Gdx.app.exit()
		return true
	}
	
	fun reloadFont(lang: String) {
		if (::font.isInitialized) font.dispose()
		if (::gameFont.isInitialized) gameFont.dispose()
		
		var font = Gdx.files.internal("$P_ass/fonts/$lang.ttf")
		if (!font.exists()) font = Gdx.files.internal("$P_ass/fonts/Arial.ttf")
		
		val generator = FreeTypeFontGenerator(font)
		var crs = Gdx.files.internal("$P_ass/fonts/$lang.crs")
		if (!crs.exists()) crs = Gdx.files.internal("$P_ass/fonts/Arial.crs")
		
		val param = FreeTypeFontGenerator.FreeTypeFontParameter()
		param.size = 18
		param.flip = true
		
		if(crs.exists()) param.characters = crs.reader().readLines()[0]
		
		this.font = generator.generateFont(param)
		this.font.data.markupEnabled = true
		
		param.size = 32
		this.gameFont = generator.generateFont(param)
		this.gameFont.data.markupEnabled = true
		
		generator.dispose()
	}
	
	fun debug(msg: Any?) = if (debug) deb(msg) else Unit
}