package alexsocol.vizion.script

interface IScript {
	
	fun perform(vararg args: String)
}