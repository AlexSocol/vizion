package alexsocol.vizion.script

import alexsocol.vizion.engine.gui.gameprocess.*
import alexsocol.vizion.engine.handler.*
import alexsocol.vizion.main
import alexsocol.vizion.util.*
import kotlin.math.absoluteValue

object ScriptBG: IScript {
	
	override fun perform(vararg args: String) {
		SceneHandler.bg = args[0]
	}
}

object ScriptSprite: IScript {
	
	override fun perform(vararg args: String) {
		
		// why there is no fallthrough in kotlin :(
		when (val subcommand = args[0]) {
			"add"    -> { SharedResources.sprite(args[1], args[2].toFloat(), args[3].toFloat()); SceneHandler.sprites.add(args[1]) }
			"delete" -> { SharedResources.deleteSprite(args[1]); SceneHandler.sprites.remove(args[1]) }
			"show"   -> SceneHandler.sprites.add(args[1])
			"hide"   -> SceneHandler.sprites.remove(args[1])
			"move"   -> SharedResources.resize(args[1], args[2].toFloat(), args[3].toFloat())
			else     -> throw IllegalArgumentException("Unknown sprite subcommand $subcommand")
		}
	}
}

object ScriptText: IScript {
	
	override fun perform(vararg args: String) {
		when (val subcommand = args[0]) {
			"speaker" -> SceneHandler.speaker = LanguageHandler.translate(args[1])
			"display" -> SceneHandler.text = LanguageHandler.translate(args[1])
			else      -> throw IllegalArgumentException("Unknown text subcommand $subcommand")
		}
	}
}

object ScriptSound: IScript {
	
	// name, pitch, panning (left/middle/right)
	override fun perform(vararg args: String) {
		SharedResources.sound(args[0]).play(SettingsHandler.volume / 100f, args.getOrElse(1) { "1" }.toFloat(), args.getOrElse(2) { "0" }.toFloat())
	}
}

object ScriptMusic: IScript {
	
	override fun perform(vararg args: String) {
		val music = SharedResources.music(args[1])
		when (args[0]) {
			"play" -> {
				music.position = args.getOrElse(3) { "0" }.toFloat()
				music.play(args.getOrElse(2) { "true" }.toBoolean())
				
				if (music.isLooping) SceneHandler.music = args[1]
			}
			"pause" -> music.pause()
			"stop" -> {
				music.stop()
				SceneHandler.music = ""
			}
		}
	}
}

object ScriptChoice: IScript {
	
	override fun perform(vararg args: String) {
		val gig = main.gui as GuiIngame
		
		if (args[0] == "-p") {
			for (i in 1 until args.size step 3) {
				if (SceneHandler.userVariables[args[i]]!!.toBoolean())
					gig.playerSelectionButtons.add(ButtonChoise(0f, 0f, 480f, 80f, args[i + 1], args[i + 2]))
			}
		} else {
			for (i in args.indices step 2)
				gig.playerSelectionButtons.add(ButtonChoise(0f, 0f, 480f, 80f, args[i], args[i + 1]))
		}
		
		gig.onResize()
	}
}

/** Loads next chapter (list of script sets). MUST be called LAST in the file */
object ScriptChapter: IScript {
	
	override fun perform(vararg args: String) {
		ScriptHandler.loadScript(args[0])
	}
}

/** Ends the game (displays [GuiEndgame]). MUST be called LAST in the file*/
object ScriptEndGame: IScript {
	
	override fun perform(vararg args: String) {
		main.ended = true
		main.setCurrentGui(GuiEndgame(args.getOrNull(0) ?: "theend" ), false)
	}
}

/** User-defined variables */
object ScriptVariable: IScript {
	
	override fun perform(vararg args: String) {
		when (args[0]) {
			"set"    -> SceneHandler.userVariables[args[1]] = args[2]
			"read"   -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[2]]!!
			"delete" -> SceneHandler.userVariables.remove(args[1])
			"print"  -> log("var ${args[1]} = ${SceneHandler.userVariables[args[1]]}")
			
			"add"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().plus (SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"sub"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().minus(SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"mul"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().times(SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"div"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().div  (SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"rem"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().rem  (SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"abs"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toDouble().absoluteValue.toString()
			
			"cmpg"   -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() >  SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"cmpl"   -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() <  SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"cmpge"  -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() >= SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"cmple"  -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() <= SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"cmpe"   -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() == SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			"cmpne"  -> SceneHandler.userVariables[args[3]] = (SceneHandler.userVariables[args[1]]!!.toDouble() != SceneHandler.userVariables[args[2]]!!.toDouble()).toString()
			
			"not"    -> SceneHandler.userVariables[args[1]] = SceneHandler.userVariables[args[1]]!!.toBoolean().not().toString()
			"or"     -> SceneHandler.userVariables[args[3]] = SceneHandler.userVariables[args[1]]!!.toBoolean().or (SceneHandler.userVariables[args[2]]!!.toBoolean()).toString()
			"xor"    -> SceneHandler.userVariables[args[3]] = SceneHandler.userVariables[args[1]]!!.toBoolean().xor(SceneHandler.userVariables[args[2]]!!.toBoolean()).toString()
			"and"    -> SceneHandler.userVariables[args[3]] = SceneHandler.userVariables[args[1]]!!.toBoolean().and(SceneHandler.userVariables[args[2]]!!.toBoolean()).toString()
		}
	}
}

/** Changes current instructions set */
object ScriptGoto: IScript {
	
	override fun perform(vararg args: String) {
		val go = if (args.size == 2) SceneHandler.userVariables[args[1]]!!.toBoolean() else true
		
		if (!go) return
		
		ScriptHandler.packIndex = ScriptHandler.labels[args[0]]!!
		(main.gui as GuiIngame).nextScene()
	}
}