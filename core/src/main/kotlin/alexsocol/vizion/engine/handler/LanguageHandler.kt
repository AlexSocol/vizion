package alexsocol.vizion.engine.handler

import alexsocol.vizion.*
import com.badlogic.gdx.Gdx

object LanguageHandler {
	
	private var translations = HashMap<String, String>()
	
	fun translate(key: String, vararg format: String) = (translations[key] ?: key).format(*format)
	
	fun readLangs(lang: String) {
		translations.clear()
		
		val fileHandle = Gdx.files.internal("$P_lang/$lang.lang")
		
		if (!fileHandle.exists()) return
		
		fileHandle.reader().readLines().forEach { line ->
			if (line.isBlank() || line.startsWith('#')) return@forEach
			
			val (k, v) = line.split("=".toRegex(), 2)
			translations[k] = v
		}
	}
}