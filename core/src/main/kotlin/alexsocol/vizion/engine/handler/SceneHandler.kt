package alexsocol.vizion.engine.handler

object SceneHandler {
	
	/**
	 * for convenience
	 */
	var chapter = "start"
	
	/**
	 * Background image name
	 */
	var bg = "default"
	
	/**
	 * Set of sprite names on scene
	 */
	var sprites = HashSet<String>()
	
	/**
	 * Currently displayed text
	 */
	var text = ""
	
	/**
	 * Currently speaking character
	 */
	var speaker = ""
	
	/**
	 * Currently playing looped music
	 */
	var music = ""
	
	/**
	 * User-defined variables for use in scripts
	 */
	val userVariables = HashMap<String, String>()
}