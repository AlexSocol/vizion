package alexsocol.vizion.engine.handler

import alexsocol.vizion.util.*
import com.badlogic.gdx.Gdx

object SavingHandler {
	
	const val autoSaveName = "bin/saves/save_-1.txt"
	
	val savesInfo = arrayOfNulls<SaveInfo?>(10)
	
	/**
	 * Read base save info for each slot (date, chapter, etc)
	 */
	fun read() {
		val saveDataHandle = Gdx.files.local("bin/saves/savedata.txt")
		if (!saveDataHandle.exists()) return
		
		saveDataHandle.reader().readLines().paired().forEachIndexed { id, pair ->
			savesInfo[id] = if (pair.first.isEmpty() || pair.second.isEmpty()) null else SaveInfo(pair.first, pair.second)
		}
	}
	
	fun write() {
		Gdx.files.local("bin/saves/savedata.txt").writeString( savesInfo.fold("") { acc, it -> "$acc${ if (it == null) "\n" else "${it.chapter}\n${it.date}" }\n"}.dropLast(1), false )
	}
	
	/**
	 * @return true on successful loading
	 */
	fun load(slot: Int): Boolean {
		val saveHandle = Gdx.files.local("bin/saves/save_$slot.txt")
		if (!saveHandle.exists()) return false.also { if (slot !=-1 ) savesInfo[slot] = null }
		
		val lines = saveHandle.reader().readLines() as ArrayList
		
		val chapter = lines.removeAt(0)
		ScriptHandler.loadScript(chapter)
		ScriptHandler.packIndex = lines.removeAt(0).toInt()
		
		SceneHandler.chapter = chapter
		SceneHandler.bg = lines.removeAt(0)
		
		SceneHandler.sprites.clear()
		lines.retainFirst(lines.removeAt(0).toInt() * 2).paired().forEach {
			SceneHandler.sprites.add(it.first)
			val (x, y) = it.second.split(' ')
			SharedResources.sprite(it.first, x.toFloat(), y.toFloat())
		}
		
		SceneHandler.text = lines.removeAt(0)
		SceneHandler.speaker = lines.removeAt(0)
		SceneHandler.music = lines.removeAt(0).also { if (it.isNotEmpty()) SharedResources.music(it).play(true) }
		
		lines.associateTo(SceneHandler.userVariables.also { it.clear() }) { it.split("=".toRegex(), 2).paired()[0] }
		
		return true
	}
	
	fun autoLoad() = load(-1)
	
	/**
	 * @return true on successful saving
	 */
	fun save(slot: Int): Boolean {
		val saveHandle = Gdx.files.local("bin/saves/save_$slot.txt")
		
		val sb = StringBuilder()
		sb.append(SceneHandler.chapter).append('\n')
		sb.append(ScriptHandler.packIndex.toString()).append('\n')
		sb.append(SceneHandler.bg).append('\n')
		sb.append(SceneHandler.sprites.size.toString()).append('\n')
		
		// TODO write ALL sprites
		SceneHandler.sprites.forEach { SharedResources.writeSprite(sb, it) }
		sb.append(SceneHandler.text).append('\n')
		sb.append(SceneHandler.speaker).append('\n')
		sb.append(SceneHandler.music).append('\n')
		
		SceneHandler.userVariables.forEach { (k, v) -> sb.append("$k=$v\n") }
		
		saveHandle.writeString(sb.toString(), false)
		
		if (slot != -1) {
			savesInfo[slot] = SaveInfo(SceneHandler.chapter, formatDate())
		}
		
		write()
		return true
	}
	
	fun autoSave() = save(-1)
	
	fun hasSave(slot: Int) = savesInfo.getOrNull(slot) != null
}

data class SaveInfo(val chapter: String, val date: String)