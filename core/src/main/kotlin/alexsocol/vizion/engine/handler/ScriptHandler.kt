package alexsocol.vizion.engine.handler

import alexsocol.vizion.*
import alexsocol.vizion.script.*
import alexsocol.vizion.util.*
import com.badlogic.gdx.Gdx

object ScriptHandler {
	
	val spaceNotInQuotes = Regex(""" (?=([^"]*"[^"]*")*[^"]*$)""")
	
	val scripts = HashMap<String, IScript>()
	
	init {
		scripts["bg"] = ScriptBG
		scripts["sprite"] = ScriptSprite
		scripts["text"] = ScriptText
		scripts["sound"] = ScriptSound
		scripts["music"] = ScriptMusic
		scripts["choice"] = ScriptChoice
		scripts["chapter"] = ScriptChapter
		scripts["end"] = ScriptEndGame
		scripts["var"] = ScriptVariable
		scripts["goto"] = ScriptGoto
	}
	
	/** list of script sets executed in one go, paired by script realization and arguments to execute */
	val scriptPacks = ArrayList<MutableList<Pair<IScript, MutableList<String>>>>()
	
	/** Map of labels for goto: <name to new [packIndex]>  */
	val labels = HashMap<String, Int>()
	
	/** index of currently executing pack */
	var packIndex = 0
	
	fun loadScript(name: String) {
		scriptPacks.also { it.forEach { list -> list.forEach { pair -> pair.second.clear() }; list.clear() }; it.clear() }
		labels.clear()
		packIndex = 0
		
		
		
		SceneHandler.chapter = name
		SceneHandler.bg = "default"
		SceneHandler.speaker = ""
		SceneHandler.text = ""
		SceneHandler.sprites.clear()
		
		
		
		val lines = Gdx.files.internal("$P_scripts/$name.vzn").reader().readLines()
		
		val iter = lines.listIterator()
		
		while (iter.hasNext()) {
			var line = iter.nextNotComment()
			
			if (line.startsWith("@")) {
				labels[line.dropWhile { it == '@' }] = packIndex
				line = iter.nextNotComment()
			}
			
			val scriptPack = ArrayList<Pair<IScript, MutableList<String>>>()
			
			while (line.isNotBlank()) {
				val args = line.split(spaceNotInQuotes).toMutableList()
				val script = args.removeAt(0)
				
				if (!scripts.containsKey(script)) {
					val error = "No script $script found\n\tat $name.vzn (line ${iter.nextIndex()})"
					
					// TODO change to dev mode
					if (main.debug) throw IllegalArgumentException(error) else err(error)
				}
				
				val li = args.listIterator()
				while (li.hasNext()) li.next().trim().dropWhile { it == '"' }.dropLastWhile { it == '"' }.also { li.set(it) }
				
				scriptPack.add(scripts[script]!! to args)
				
				line = if (iter.hasNext()) iter.nextNotComment() else ""
			}
			
			if (scriptPack.isNotEmpty()) {
				scriptPacks.add(scriptPack)
				packIndex++
			}
		}
		
		packIndex = 0
	}
	
	fun ListIterator<String>.nextNotComment(): String {
		var line: String
		
		do {
			line = next()
		} while (line.startsWith("#") || line.startsWith("//"))
		
		return line
	}
}

