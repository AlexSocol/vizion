package alexsocol.vizion.engine.handler

import alexsocol.vizion.*
import alexsocol.vizion.util.*
import com.badlogic.gdx.Gdx

object SettingsHandler {
	
	var width: Float
		get() = settings["width"]?.toFloat() ?: 1280f
		set(value) {
			settings["width"] = value.toString()
		}
	
	var height: Float
		get() = settings["height"]?.toFloat() ?: 720f
		set(value) {
			settings["height"] = value.toString()
		}
	
	var volume: Int
		get() = settings["volume"]?.toInt() ?: 50
		set(value) {
			settings["volume"] = value.toString()
			SharedResources.adjustVolume(value)
		}
	
	var fullscreen: Boolean
		get() = settings["fullscreen"]?.toBoolean() ?: false
		set(value) {
			settings["fullscreen"] = value.toString()
			
			if (value)
				Gdx.graphics.setFullscreenMode(Gdx.graphics.displayMode)
			else
				Gdx.graphics.setWindowedMode(width.toInt(), height.toInt())
		}
	
	var lang: String
		get() = settings["language"] ?: "en_US"
		set(value) {
			settings["language"] = value
			
			LanguageHandler.readLangs(value)
			main.reloadFont(value)
		}
	
	val settings = HashMap<String, String>()
	
	fun read() {
		val fileHandle = Gdx.files.local("bin/settings.txt")
		
		if (fileHandle.exists()) {
			fileHandle.file().readLines().forEach { line ->
				if (line.isBlank()) return@forEach
				
				val (k, v) = line.split("=".toRegex(), 2)
				settings[k] = v
			}
		}
		
		resize(width.toInt(), height.toInt(), fullscreen)
	}
	
	fun resize(w: Int, h: Int, full: Boolean) {
		width = w.toFloat()
		height = h.toFloat()
		
		fullscreen = full
		
		main.camera.setToOrtho(true, width, height)
		main.gui.onResize()
		SharedResources.resizeSprites()
		main.prevGui?.onResize()
	}
	
	fun write() {
		Gdx.files.local("bin").mkdirs()
		val fileHandle = Gdx.files.local("bin/settings.txt")
		
		if (!fileHandle.exists()) fileHandle.file().createNewFile()
		
		fileHandle.writeString(settings.toList().fold("") { acc, (k, v) -> "$acc$k=$v\n" }, false)
	}
	
	override fun toString() = settings.toString()
}