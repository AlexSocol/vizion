package alexsocol.vizion.engine.handler

import alexsocol.vizion.main
import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.Input.Keys.*
import com.badlogic.gdx.InputAdapter

class InputHandler: InputAdapter() {
	
	override fun keyDown(keycode: Int): Boolean {
		val pre = main.gui.onKeyDown(keycode)
		
		if (pre) return pre
		
		return when (keycode) {
			F3     -> true.also { main.debug = !main.debug }
			F11    -> true.also { SettingsHandler.fullscreen = !SettingsHandler.fullscreen }
			ESCAPE -> main.prevGui()
			else   -> false
		}
	}
	
	override fun touchUp(x: Int, y: Int, pointer: Int, button: Int) = if (button == Buttons.LEFT) main.gui.onClicked(x, y) else false
}
