package alexsocol.vizion.engine.gui.button

//import alexsocol.vizion.getMain
//import com.badlogic.gdx.Gdx
//import com.badlogic.gdx.graphics.Color
//
//class ButtonColored(x: Float, y: Float, w: Float, h: Float, text: String, private val selBut: Color, private val selTxt: Color, private val clkBut: Color, private val clkTxt: Color, private val regTxt: Color, action: () -> Boolean) : Button(x, y, w, h, text, action) {
//
//	override fun draw(texture: String) {
//		val hovering = within(Gdx.input.x, Gdx.input.y)
//		val clicked = hovering && Gdx.input.isTouched
//
//		val cb = main.batch.color.cpy()
//		val cf = main.font.color.cpy()
//
//		main.batch.color = if (clicked) clkBut else if (hovering) selBut else main.batch.color
//		main.batch.draw(main.res.img(texture), xs(), ys(), w, h)
//
//		main.font.color = if (clicked) clkTxt else if (hovering) selTxt else regTxt
//		main.glyph.setText(main.font, text)
//		main.font.draw(main.batch, main.glyph, x - main.glyph.width / 2, y - main.glyph.height / 2)
//
//		main.batch.color = cb
//		main.font.color = cf
//	}
//}