package alexsocol.vizion.engine.gui.saving

import alexsocol.vizion.*
import alexsocol.vizion.engine.handler.SavingHandler
import com.badlogic.gdx.*

class GuiSave: GuiSaveOrLoad("save", { true }) {
	
	override fun onLoad() {
		super.onLoad()
		
		val flag = Gdx.files.local(SavingHandler.autoSaveName).exists()
		buttonList.add(ButtonSaveLoad({ slot == -1 && flag }, { warn }, "save", "quicksave", { save() }))
	}
	
	private fun save(): Boolean {
		return if (slot == -1)
			SavingHandler.autoSave().also {
				main.prevGui()
			} else confirm()
	}
	
	private fun confirm(): Boolean {
		if (SavingHandler.hasSave(slot)) {
			if (!warn) {
				warn = true
				return false
			} else warn = false
		}

		return SavingHandler.save(slot).also {
			main.prevGui()
		}
	}
	
	override fun onResize() {
		super.onResize()
		
		// SAVE button
		buttonList[11].x = main.settings.width / 2f + 200f
		buttonList[11].y = main.settings.height - 50f
	}
}