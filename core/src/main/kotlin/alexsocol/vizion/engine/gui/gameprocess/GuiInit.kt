package alexsocol.vizion.engine.gui.gameprocess

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.GuiWithImage
import alexsocol.vizion.engine.handler.*
import com.badlogic.gdx.graphics.Color

/**
 * Gui on game load
 */
class GuiInit		: GuiWithImage("init") {
	
	override fun onClicked(x: Int, y: Int): Boolean {
		main.setCurrentGui(GuiMainMenu(), false)
		return true
	}
	
	override fun draw() {
		super.draw()
		
		val stage = when(main.initStage) {
			1 -> "Reading settings..."
			2 -> "Reading saves..."
			3 -> "Parsing lang..."
			4 -> "Reloading fonts..."
			else -> LanguageHandler.translate("init.done")
		}
		
		val w = SettingsHandler.width / 1920f
		val h = SettingsHandler.height / 1080f
		val was = main.font.color.cpy()
		main.font.color = Color.BLACK
		if (main.initStage != main.maxStages) main.font.draw(main.batch, "Now loading... ${main.initStage}/${main.maxStages}", 25f, 800f * h)
		main.font.draw(main.batch, stage, 25f, 820f * h)
		
		main.glyph.setText(main.font, copyright)
		main.font.draw(main.batch, main.glyph, 1920 * w - main.glyph.width - 20, 1080 * h - main.glyph.height - 20)
		
		main.font.color = was
		
		
	}
}