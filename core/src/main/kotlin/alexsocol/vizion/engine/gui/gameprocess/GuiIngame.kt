package alexsocol.vizion.engine.gui.gameprocess

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.GuiWithImageAndButtons
import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.engine.gui.saving.*
import alexsocol.vizion.engine.gui.settings.GuiSettings
import alexsocol.vizion.engine.handler.*
import alexsocol.vizion.util.*
import com.badlogic.gdx.Input.Keys.*
import com.badlogic.gdx.utils.Align
import java.io.Console

class GuiIngame: GuiWithImageAndButtons("logo", "button") {
	
	val playerSelectionButtons = ArrayList<ButtonChoise>()
	
	var hideText = false
	
	override fun onLoad() {
		super.onLoad()
		
		main.ended = false
		
		buttonList.add(Button(0f, 0f, 120f, 20f, "save") { openSaves() })
		buttonList.add(Button(0f, 0f, 120f, 20f, "load") { openLoads() })
		buttonList.add(Button(0f, 0f, 120f, 20f, "settings") { openSettings() })
		buttonList.add(Button(0f, 0f, 120f, 20f, "menu") { mainMenu() })
	}
	
	override fun draw() {
		processConsole()
		
		drawBg(SharedResources.bg(SceneHandler.bg))
		
		SceneHandler.sprites.forEach {
			SharedResources.sprite(it).draw(main.batch)
		}
		
		if (playerSelectionButtons.isNotEmpty()) {
			playerSelectionButtons.forEach { it.draw(buttonTextureName) }
			return
		}
		
		val text = SceneHandler.text
		val speaker = SceneHandler.speaker
		
		if (!hideText && (text.isNotEmpty() || speaker.isNotEmpty())) {
			val font = main.gameFont
			
			val off = 5 * main.settings.height / 1080
			
			if (speaker.isNotEmpty()) {
				drawBg(SharedResources.textAreaWithSpeaker)
				font.draw(main.batch, LanguageHandler.translate("speaker.$speaker"), off, main.settings.height - (393 * main.settings.height / 1080))
			} else
				drawBg(SharedResources.textArea)
			
			font.draw(main.batch, LanguageHandler.translate("text.$text"), off, main.settings.height - (348 * main.settings.height / 1080), main.settings.width - off, Align.topLeft, true)
		}
		
		buttonList.forEach { it.draw(buttonTextureName) }
	}
	
	override fun onKeyDown(keycode: Int): Boolean {
		if (playerSelectionButtons.isNotEmpty()) return false
		
		return when (keycode) {
			SPACE -> nextScene()
			F5    -> hotSave()
			F9    -> hotLoad()
			O     -> openSaves()
			L     -> openLoads()
			S     -> openSettings()
			M     -> mainMenu()
			H     -> { hideText = !hideText; return true }
			else  -> false
		}
	}
	
	override fun onClicked(x: Int, y: Int): Boolean {
		if (playerSelectionButtons.isNotEmpty()) {
			playerSelectionButtons.forEach {
				if (it.within(x, y) && it.onClick()) return true
			}
			
			return true // prevent further processing
		}
		
		return super.onClicked(x, y) || nextScene()
	}
	
	fun nextScene(): Boolean {
		val id = if (ScriptHandler.scriptPacks.size <= ScriptHandler.packIndex) ScriptHandler.scriptPacks.size - 1 else ScriptHandler.packIndex++
		
		ScriptHandler.scriptPacks[id].forEach {
			if (!main.ended) it.first.perform(*it.second.toTypedArray())
		}
		return true
	}
	
	fun hotSave(): Boolean {
		return SavingHandler.autoSave()
	}
	
	fun hotLoad(): Boolean {
		return SavingHandler.autoLoad()
	}
	
	fun openSaves(): Boolean {
		main.setCurrentGui(GuiSave())
		return true
	}
	
	fun openLoads(): Boolean {
		main.setCurrentGui(GuiLoad())
		return true
	}
	
	fun openSettings(): Boolean {
		main.setCurrentGui(GuiSettings())
		return true
	}
	
	fun mainMenu(): Boolean {
		hotSave()
		main.setCurrentGui(GuiMainMenu(), false)
		return true
	}
	
	override fun onResize() {
		super.onResize()
		
		var w = -185
		buttonList.forEach {
			it.x = main.settings.width / 2f + w
			it.y = 35f
			w += 125
		}
		
		if (playerSelectionButtons.isNotEmpty()) {
			var y = 100f
			playerSelectionButtons.forEach {
				it.x = main.settings.width / 2f
				it.y = y
				y += it.h + 10
			}
		}
	}
	
	fun processConsole() {
		if (System.`in`.available() <= 0) return
		
		val line = readLine() ?: return
		
		val args = line.split(ScriptHandler.spaceNotInQuotes).toMutableList()
		val script = args.removeAt(0)
		
		if (!ScriptHandler.scripts.containsKey(script)) {
			return err("No script $script found")
		}
		
		val li = args.listIterator()
		while (li.hasNext()) li.next().trim().dropWhile { it == '"' }.dropLastWhile { it == '"' }.also { li.set(it) }
		
		try {
			ScriptHandler.scripts[script]!!.perform(*args.toTypedArray())
		} catch (err: Throwable) {
			err(err)
		}
	}
	
	override fun onDispose() {
		super.onDispose()
		
		playerSelectionButtons.clear()
	}
}