package alexsocol.vizion.engine.gui.button

import alexsocol.vizion.main
import alexsocol.vizion.engine.handler.LanguageHandler
import alexsocol.vizion.util.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color

val LIGHT_BLUE = Color(-2133524481) // 0x80d4ffff <- kotlin can't in hex RGBA :(

/**
 * @param x center position
 * @param y center position
 * @param w button width
 * @param h button height
 */
open class Button(var x: Float, var y: Float, val w: Float, val h: Float, var text: String, val action: () -> Boolean) {
	// constructor() : this(0f, 0f, 0f, 0f, "", { false } )
	
	var translatePrefix = "button."
	
	/** Top-left corner x */
	protected fun xs() = x-w/2f
	/** Top-left corner y */
	protected fun ys() = y-h/2f
	
	/** Bottom-right corner x */
	private fun xm() = x+w/2f
	/** Bottom-right corner y */
	private fun ym() = y+h/2f
	
	/**
	 * Is point (px, py) within button bounds
	 * (is button selected by mouse)
	 */
	fun within(px: Int, py: Int) = px.toFloat() / main.width * main.settings.width in xs()..xm() &&
								   py.toFloat() / main.height * main.settings.height in ys()..ym()
	
	open fun draw(texture: String) {
		val hovering = within(Gdx.input.x, Gdx.input.y)
		val clicked = hovering && Gdx.input.isTouched
		
		val prevBatchColor = main.batch.color.cpy()
		
		main.batch.color = if (clicked) LIGHT_BLUE else if (hovering) main.batch.color.fade(0.9f) else main.batch.color
		main.batch.draw(SharedResources.img("gui/$texture"), xs(), ys(), w, h)
		
		if (text.isNotBlank()) {
			val prevFontColor = main.font.color.cpy()
			
			main.font.color = if (clicked) Color.YELLOW else if (hovering) Color.WHITE else Color.LIGHT_GRAY
			main.glyph.setText(main.font, LanguageHandler.translate("$translatePrefix$text"))
			main.font.draw(main.batch, main.glyph, x - main.glyph.width / 2, y - main.glyph.height / 2)
			
			main.font.color = prevFontColor
		}
		
		main.batch.color = prevBatchColor
	}
	
	open fun onClick() = action()
}