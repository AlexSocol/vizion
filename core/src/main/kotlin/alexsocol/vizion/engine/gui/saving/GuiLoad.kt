package alexsocol.vizion.engine.gui.saving

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.gameprocess.*
import alexsocol.vizion.engine.handler.SavingHandler
import com.badlogic.gdx.*

class GuiLoad: GuiSaveOrLoad("load", { SavingHandler.savesInfo[it] != null }) {
	
	override fun onLoad() {
		super.onLoad()
		
		val flag = Gdx.files.local(SavingHandler.autoSaveName).exists()
		buttonList.add(ButtonSaveLoad({ slot == -1 && flag }, { warn }, "load", "quickload", { load() }))
	}
	
	fun load() =
		if (slot == -1) hotLoad()
		else {
			if (main.prevGui is GuiIngame) {
				if (!warn) {
					warn = true
					false
				} else {
					warn = false
					SavingHandler.load(slot).also { main.prevGui() }
				}
			} else SavingHandler.load(slot).also { main.setCurrentGui(GuiIngame(), false) }
		}
	
	fun hotLoad(): Boolean {
		return SavingHandler.autoLoad().also {
			if (main.prevGui is GuiMainMenu)
				main.setCurrentGui(GuiIngame(), false)
			else if (main.prevGui is GuiIngame)
				main.prevGui()
		}
	}
	
	override fun onResize() {
		super.onResize()
		
		// LOAD button
		buttonList[11].x = main.settings.width / 2f + 200f
		buttonList[11].y = main.settings.height - 50f
	}
}