package alexsocol.vizion.engine.gui.gameprocess

import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.engine.handler.ScriptHandler
import alexsocol.vizion.main

class ButtonChoise(x: Float, y: Float, w: Float, h: Float, text: String, val anchor: String): Button(x, y, w, h, text, { true }) {
	
	init {
		translatePrefix = "option."
	}
	
	override fun onClick(): Boolean {
		ScriptHandler.packIndex = ScriptHandler.labels[anchor]!!
		(main.gui as GuiIngame).also {
			it.nextScene()
			it.playerSelectionButtons.clear()
		}
		
		return super.onClick()
	}
}
