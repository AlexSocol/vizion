package alexsocol.vizion.engine.gui.saving

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.button.Button
import com.badlogic.gdx.graphics.Color

// button from save/load confirmation
class ButtonSaveLoad(private val checkSlot: () -> Boolean, private val checkWarn: () -> Boolean, private val txt: String, private val newText: String, action: () -> Boolean): Button(0f, 0f, 360f, 60f, txt, action) {
	
	override fun draw(texture: String) {
		text = if (checkSlot()) newText else txt
		
		val prevColor = main.batch.color.cpy()
		if (checkWarn()) main.batch.color = Color.SCARLET
		
		super.draw(texture)
		main.batch.color = prevColor
	}
}