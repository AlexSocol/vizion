package alexsocol.vizion.engine.gui

import alexsocol.vizion.main
import alexsocol.vizion.util.*

/**
 * Gui only with single image
 */
abstract class GuiWithImage(val imageName: String) : IGui {
	override fun onLoad() {}
	
	override fun onResize() {}
	
	override fun onClicked(x: Int, y: Int) = false
	
	override fun onDispose() {}
	
	override fun draw() = drawBg(SharedResources.img(imageName))
}