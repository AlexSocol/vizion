package alexsocol.vizion.engine.gui.settings

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.button.*
import alexsocol.vizion.engine.handler.LanguageHandler
import alexsocol.vizion.util.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import kotlin.math.*

class ButtonSlider(x: Float, y: Float, w: Float, h: Float, private val min: Int, private val max: Int, private val change: MutableInt, text: String): Button(x, y, w, h, text, { true }) {
	
	override fun draw(texture: String) {
		val hovering = within(Gdx.input.x, Gdx.input.y)
		val clicked = hovering && Gdx.input.isTouched
		
		val cb = main.batch.color.cpy()
		val cf = main.font.color.cpy()
		
		main.batch.draw(SharedResources.img("gui/$texture"), xs(), ys(), w, h)
		
		main.font.color = if (clicked) Color.YELLOW else if (hovering) Color.WHITE else Color.LIGHT_GRAY
		main.glyph.setText(main.font, "${LanguageHandler.translate("slider.$text")}: ${change.i}")
		main.font.draw(main.batch, main.glyph, x - main.glyph.width / 2, y - main.glyph.height / 2)
		
		main.batch.color = if (clicked) LIGHT_BLUE else if (hovering) main.batch.color.fade(0.9f) else main.batch.color
		main.batch.draw(SharedResources.img("gui/scroller"), clamp(clicked), ys(), h * h / w * 2, h)
		
		main.batch.color = cb
		main.font.color = cf
	}
	
	private fun clamp(b: Boolean): Float {
		val mx = w - h * h / w * 2
		val pos = if (b) max(xs(), min(Gdx.input.x / main.width * main.settings.width, xs() + mx)) else xs() + mx * change.i / (max - min).toFloat()
		if (b) change.i = mapRange(0f, mx, min.toFloat(), max.toFloat(), pos - xs()).toInt()
		return pos
	}
}