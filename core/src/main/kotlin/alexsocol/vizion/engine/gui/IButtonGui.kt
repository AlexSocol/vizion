package alexsocol.vizion.engine.gui

import alexsocol.vizion.engine.gui.button.Button

/**
 * Gui with buttons interface
 */
interface IButtonGui : IGui {
	
	override fun onClicked(x: Int, y: Int): Boolean {
		getButtons().forEach {
			if (it.within(x, y) && it.onClick())
				return true
		}
		
		return false
	}
	
	fun getButtons(): MutableList<Button>
}