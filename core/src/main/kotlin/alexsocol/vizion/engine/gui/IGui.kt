package alexsocol.vizion.engine.gui

/**
 * Simple Gui interface
 */
interface IGui {
	fun onLoad()
	fun onResize()
	fun onClicked(x: Int, y: Int): Boolean
	fun onKeyDown(keycode: Int): Boolean = false
	fun onDispose()
	fun draw()
}