package alexsocol.vizion.engine.gui.gameprocess

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.*
import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.engine.gui.saving.GuiLoad
import alexsocol.vizion.engine.gui.settings.GuiSettings
import alexsocol.vizion.engine.handler.*
import com.badlogic.gdx.*
import com.badlogic.gdx.Input.Keys.*

/**
 * Main menu gui
 */
class GuiMainMenu: GuiWithImageAndButtons("logo", "button") {
	
	override fun onLoad() {
		super.onLoad()
		
		if (Gdx.files.local(SavingHandler.autoSaveName).exists())
			buttonList.add(Button(0f, 0f, 240f, 40f, "quickload") { hotLoad() })
		
		buttonList.add(Button(0f, 0f, 240f, 40f, "newgame") { start() })
		buttonList.add(Button(0f, 0f, 240f, 40f, "load") { openLoads() })
		buttonList.add(Button(0f, 0f, 240f, 40f, "settings") { openSettings() })
		buttonList.add(Button(0f, 0f, 240f, 40f, "quit") { quit() })
	}
	
	override fun onKeyDown(keycode: Int): Boolean {
		return when (keycode) {
			F9   -> hotLoad()
			G    -> start()
			L    -> openLoads()
			S    -> openSettings()
			Q    -> quit()
			else -> false
		}
	}
	
	fun start(): Boolean {
		val gig = GuiIngame()
		main.setCurrentGui(gig, false)
		
		ScriptHandler.loadScript("start")
		gig.nextScene()
		
		return true
	}
	
	fun hotLoad(): Boolean {
		return SavingHandler.autoLoad().also {
			main.setCurrentGui(GuiIngame(), false)
		}
	}
	
	fun openLoads(): Boolean {
		main.setCurrentGui(GuiLoad())
		return true
	}
	
	fun openSettings(): Boolean {
		main.setCurrentGui(GuiSettings())
		return true
	}
	
	fun quit(): Boolean {
		return main.quit()
	}
	
	override fun onResize() {
		super.onResize()
		var offset = if (Gdx.files.local(SavingHandler.autoSaveName).exists()) -50f else 0f
		
		buttonList.forEach {
			it.x = main.settings.width / 2f
			it.y = main.settings.height / 2f + offset
			offset += 50
		}
	}
}