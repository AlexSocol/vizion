package alexsocol.vizion.engine.gui

import alexsocol.vizion.engine.gui.button.Button

abstract class GuiWithImageAndButtons(image: String, val buttonTextureName: String) : GuiWithImage(image), IButtonGui {
	
	open val buttonList = ArrayList<Button>()
	
	override fun onClicked(x: Int, y: Int): Boolean = super<IButtonGui>.onClicked(x, y)
	
	override fun draw() {
		super.draw()
		buttonList.forEach { it.draw(buttonTextureName) }
	}
	
	override fun getButtons() = buttonList
}