package alexsocol.vizion.engine.gui.saving

import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.util.fade
import alexsocol.vizion.main
import com.badlogic.gdx.graphics.Color

/**
 * Button for save slots
 */
class SaveSlot(x: Float, y: Float, val id: Int, action: () -> Boolean): Button(x, y, 360f, 60f, "", action) {
	
	init {
		translatePrefix = ""
	}
	
	override fun draw(texture: String) {
		val prevColor = main.batch.color.cpy()
		
		if (main.gui is GuiSaveOrLoad && id == (main.gui as GuiSaveOrLoad).slot) main.batch.color = if ((main.gui as GuiSaveOrLoad).warn) Color.SCARLET else Color.LIME.fade(3f)
		super.draw(texture)
		
		main.batch.color = prevColor
	}
}