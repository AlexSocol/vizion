package alexsocol.vizion.engine.gui.button

open class ButtonTextured(x: Float, y: Float, w: Float, h: Float, text: String, private val texture: String, action: () -> Boolean) : Button(x, y, w, h, text, action) {
	override fun draw(texture: String) = super.draw(this.texture)
}