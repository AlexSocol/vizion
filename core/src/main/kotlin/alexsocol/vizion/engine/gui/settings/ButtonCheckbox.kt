package alexsocol.vizion.engine.gui.settings

import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.main
import alexsocol.vizion.util.*

class ButtonCheckbox(x: Float, y: Float, w: Float, h: Float, private val change: MutableBoolean, text: String): Button(x, y, w, h, text, { true }) {
	
	override fun onClick(): Boolean {
		change.b = !change.b
		return super.onClick()
	}
	
	override fun draw(texture: String) {
		super.draw("checkbox")
		if (change.b) main.batch.draw(SharedResources.img("gui/check"), xs(), ys(), w, h)
	}
}