package alexsocol.vizion.engine.gui.saving

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.GuiWithImageAndButtons
import alexsocol.vizion.engine.gui.button.Button
import alexsocol.vizion.engine.handler.*

abstract class GuiSaveOrLoad(bg: String, val canSelectCheck: (Int) -> Boolean): GuiWithImageAndButtons(bg, "slot") {
	
	/** is set when some save slot is selected */
	var slot = -1
	var warn = false
	
	override fun onLoad() {
		super.onLoad()
		
		for (i in 0..9)
			buttonList.add(SaveSlot(0f, 0f, i) {
				if (canSelectCheck(i)) {
					slot = i
					warn = false
					true
				} else false
			})
		
		buttonList.add(Button(0f, 0f, 360f, 60f, "cancel") { main.prevGui() })
	}
	
	override fun onResize() {
		val h = main.settings.height / 2f
		
		for (i in -2..2) {
			buttonList[i + 2].x = main.settings.width / 2f - 200f
			buttonList[i + 2].y = h + 70 * i
		}
		for (i in -2..2) {
			buttonList[i + 7].x = main.settings.width / 2f + 200f
			buttonList[i + 7].y = h + 70 * i
		}
		
		// CANCEL button
		buttonList[10].x = main.settings.width / 2f - 200f
		buttonList[10].y = main.settings.height - 50f
	}
	
	override fun draw() {
		buttonList.forEach {
			if (it is SaveSlot) {
				 val s = SavingHandler.savesInfo[it.id]
				 if (s != null)
				 	it.text = "\"${s.chapter}\""
				 else
				 	it.text = "slot.save.empty.${it.id}"
			}
		}
		
		super.draw()

		val h = SettingsHandler.height / 2f
		val save = LanguageHandler.translate("slot.save.name")
		for (i in -2..2) {
			val s = SavingHandler.savesInfo[(buttonList[i + 2] as SaveSlot).id] ?: continue
			
			val d = s.date.split(" ")
			main.font.draw(main.batch, "$save:", main.settings.width / 2f - 370f, h + 70 * i - 20)
			main.font.draw(main.batch, d[0], main.settings.width / 2f - 100f, h + 70 * i - 20)
			
			main.font.draw(main.batch, "${(buttonList[i+2] as SaveSlot).id}", main.settings.width / 2f - 370f, h + 70 * i + 5)
			main.font.draw(main.batch, d[1], main.settings.width / 2f - 100f, h + 70 * i + 5)
			
		}

		for (i in -2..2) {
			val s = SavingHandler.savesInfo[(buttonList[i + 7] as SaveSlot).id] ?: continue
			
			val d = s.date.split(" ")
			main.font.draw(main.batch, "$save:", main.settings.width / 2f + 30f, h + 70 * i - 20)
			main.font.draw(main.batch, d[0], main.settings.width / 2f + 300f, h + 70 * i - 20)
			
			main.font.draw(main.batch, "${(buttonList[i+7] as SaveSlot).id}", main.settings.width / 2f + 30f, h + 70 * i + 5)
			main.font.draw(main.batch, d[1], main.settings.width / 2f + 300f, h + 70 * i + 5)
		}
	}
}