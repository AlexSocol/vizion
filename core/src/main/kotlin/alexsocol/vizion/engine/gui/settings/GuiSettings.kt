package alexsocol.vizion.engine.gui.settings

import alexsocol.vizion.*
import alexsocol.vizion.engine.gui.GuiWithImageAndButtons
import alexsocol.vizion.engine.gui.button.*
import alexsocol.vizion.util.*
import alexsocol.vizion.engine.handler.*
import com.badlogic.gdx.Gdx

class GuiSettings: GuiWithImageAndButtons("settings", "button") {
	
							// name to (w to h)
	private val resolutions: Array<Pair<String, Pair<Int, Int>>> = arrayOf(
								Pair("SD", Pair(720, 480)),
								Pair("HD", Pair(1280, 720)),
								Pair("WXGA", Pair(1366, 768)),
								Pair("FHD", Pair(1920, 1080)),
								Pair("UHD", Pair(3840, 2160)))
	
	private val langs = Gdx.files.internal("$P_lang/langs.txt").reader().readLines()
	
	private var resolutionPointer = 1
		set(value) {
			field = if (value >= resolutions.size) 0 else if (value < 0) resolutions.size - 1 else value
		}
	
	private var langPointer = 0
		set(value) {
			field = if (value >= langs.size) 0 else if (value < 0) langs.size - 1 else value
		}
	
	init {
		
		var i = resolutions.indexOfFirst { it.second.first == main.settings.width.toInt() }
		if (i != -1) resolutionPointer = i
		
		i = langs.indexOfFirst { it == main.settings.lang }
		if (i != -1) langPointer = i
	}
	
	private val fullscreen = MutableBoolean(main.settings.fullscreen)
	private val volume = MutableInt(main.settings.volume)
	
	override fun onLoad() {
		super.onLoad()
		buttonList.add(Button(0f, 0f, 240f, 40f, "cancel") { main.prevGui() })
		buttonList.add(Button(main.settings.width / 2, main.settings.height - 40f, 240f, 40f, "done") { apply() })
		
		buttonList.add(ButtonTextured(0f, 0f, 32f, 32f, "next", "quad") { ++resolutionPointer; true })
		buttonList.add(ButtonTextured(0f, 0f, 32f, 32f, "prev", "quad") { --resolutionPointer; true })
		
		buttonList.add(ButtonSlider(0f, 0f, 240f, 40f, 0, 100, volume, "volume"))
		buttonList.add(ButtonCheckbox(0f, 0f, 120f, 20f, fullscreen, "fullscreen"))
		
		buttonList.add(ButtonTextured(0f, 0f, 32f, 32f, "next", "quad") { ++langPointer; true })
		buttonList.add(ButtonTextured(0f, 0f, 32f, 32f, "prev", "quad") { --langPointer; true })
		
		onResize()
	}
	
	private fun apply(): Boolean {
		main.settings.resize(resolutions[resolutionPointer].second.first, resolutions[resolutionPointer].second.second, fullscreen.b)
		main.settings.volume = volume.i
		
		main.settings.lang = langs[langPointer]
		
		main.settings.write()
		
		return main.prevGui()
	}
	
	override fun onResize() {
		// CANCEL button
		buttonList[0].x = main.settings.width / 2 - 150
		buttonList[0].y = main.settings.height - 40
		// DONE button
		buttonList[1].x = main.settings.width / 2 + 150
		buttonList[1].y = main.settings.height - 40
		
		// Next res
		buttonList[2].x = main.settings.width / 2 + 80
		buttonList[2].y = main.settings.height / 2
		// Prev res
		buttonList[3].x = main.settings.width / 2 - 80
		buttonList[3].y = main.settings.height / 2
		
		// Volume
		buttonList[4].x = main.settings.width / 2
		buttonList[4].y = main.settings.height / 2 + 50
		
		// Fullscreen
		buttonList[5].x = main.settings.width / 2
		buttonList[5].y = main.settings.height / 2 - 40
		
		// Next lang
		buttonList[6].x = main.settings.width / 2 + 80
		buttonList[6].y = main.settings.height / 2 + 100
		// Prev lang
		buttonList[7].x = main.settings.width / 2 - 80
		buttonList[7].y = main.settings.height / 2 + 100
	}
	
	override fun draw() {
		super.draw()
		
		main.glyph.setText(main.font, resolutions[resolutionPointer].first)
		main.font.draw(main.batch, main.glyph, main.settings.width / 2 - main.glyph.width / 2, main.settings.height / 2 - main.glyph.height - 5)
		
		main.glyph.setText(main.font, "${resolutions[resolutionPointer].second.first}x${resolutions[resolutionPointer].second.second}")
		main.font.draw(main.batch, main.glyph, main.settings.width / 2 - main.glyph.width / 2, main.settings.height / 2 + main.glyph.height - 5)
		
		
		
		main.glyph.setText(main.font, LanguageHandler.translate("text.language"))
		main.font.draw(main.batch, main.glyph, main.settings.width / 2 - main.glyph.width / 2, main.settings.height / 2 - main.glyph.height - 5 + 100)
		
		main.glyph.setText(main.font, langs.getOrElse(langPointer) { "xx_XX" } )
		main.font.draw(main.batch, main.glyph, main.settings.width / 2 - main.glyph.width / 2, main.settings.height / 2 + main.glyph.height - 5 + 100)
	}
}