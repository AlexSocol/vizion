package alexsocol.vizion.engine.gui.gameprocess

import alexsocol.vizion.engine.gui.*
import alexsocol.vizion.engine.handler.LanguageHandler
import alexsocol.vizion.main

class GuiEndgame(val message: String): GuiWithImage("end") {
	
	override fun onClicked(x: Int, y: Int): Boolean {
		main.setCurrentGui(GuiMainMenu(), false)
		return true
	}
	
	override fun draw() {
		super.draw()
		
		main.glyph.setText(main.gameFont, LanguageHandler.translate("endscreen.$message"))
		main.gameFont.draw(main.batch, main.glyph, main.settings.width.minus(main.glyph.width) / 2, main.settings.height * 0.75f)
	}
}